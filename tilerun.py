#!/usr/bin/python
import sys, pygame, random, heapq
if not pygame.font: print ('font disabled')
from tiles import Tile as t

pygame.init()

clock = pygame.time.Clock()

# get those rotation values for directions
d_n = 0
d_w = 90
d_s = 180
d_e = 270

#some colours
red = 255, 0, 0
green = 0, 255, 0
blue = 0, 0, 255
yellow = 0, 255, 255
black = 0, 0, 0
white = 255, 255, 255
grey = 128, 128, 128  
lgrey = 200,200,200 

# basic texture loading 
t_grass = pygame.image.load('5.png')
t_rock = pygame.image.load('6.png')
t_stop = pygame.image.load('4.png')
charac = pygame.image.load('pl.png')

#tiles have an type of int - easy lookup for textures t_types[x]
t_types = [t_rock,t_rock,t_rock,t_rock,t_rock,t_grass,t_stop]

# setup map        
map = [
        [t(6), t(6), t(6), t(6), t(6), t(6), t(6), t(6), t(6), t(6), t(6), t(6), t(6), t(6), t(6)],
        [t(6), t(5), t(5), t(5), t(5), t(5), t(5), t(5), t(5), t(5), t(5), t(5), t(5), t(5), t(6)],
        [t(6), t(5), t(6), t(6), t(6), t(5), t(5), t(5), t(5), t(6), t(6), t(5), t(5), t(5), t(6)],
        [t(6), t(5), t(6), t(5), t(5), t(5), t(5), t(6), t(5), t(6), t(5), t(5), t(5), t(6), t(6)],
        [t(6), t(5), t(6), t(6), t(6), t(5), t(5), t(6), t(6), t(6), t(6), t(5), t(5), t(6), t(6)],
        [t(6), t(5), t(5), t(5), t(5), t(5), t(5), t(5), t(5), t(5), t(5), t(5), t(5), t(5), t(6)],
        [t(6), t(6), t(6), t(5), t(6), t(6), t(5), t(5), t(6), t(5), t(6), t(6), t(5), t(5), t(6)],
        [t(6), t(5), t(5), t(5), t(6), t(6), t(5), t(5), t(6), t(5), t(6), t(6), t(5), t(5), t(6)],
        [t(6), t(6), t(6), t(5), t(5), t(5), t(5), t(6), t(6), t(5), t(5), t(5), t(5), t(5), t(6)],
        [t(6), t(6), t(6), t(5), t(5), t(5), t(5), t(5), t(5), t(5), t(5), t(5), t(5), t(5), t(6)],
        [t(6), t(6), t(6), t(5), t(5), t(5), t(5), t(5), t(6), t(5), t(5), t(5), t(5), t(5), t(6)],
        [t(6), t(6), t(6), t(6), t(6), t(6), t(6), t(6), t(6), t(6), t(6), t(6), t(6), t(6), t(6)]
       ]


base_tile_size = 32        
#tilewidth
wh = base_tile_size, base_tile_size
#ofset for map <- what was I doing here? margins I think
hoff = 50
voff = 50
#gui height
guiheight = 100

mapoff = voff + guiheight # proper offset to allow for gui


#total map height and width
mapheight = len(map)*wh[0]
mapwidth = len(map[0])*wh[1]
mapleft = hoff
maptop = guiheight+voff
mapright = hoff+mapwidth
mapbottom = guiheight+voff+mapheight

print ("mapheight = %d" % (len(map)))
print ("mapwidth = %d" % (len(map[0])))
    
screenheight = mapheight + guiheight + (voff *2)
screenwidth = mapwidth + (hoff * 2)
#size for window
size= screenwidth, screenheight

#initialise screen
screen=pygame.display.set_mode(size)


gu = pygame.Surface((screenwidth, guiheight))
gusq = gu.get_rect()
gu.fill(grey)

if pygame.font:
    font = pygame.font.Font(None, 36)
    tiletext = pygame.font.Font(None, 15)


#setup surface for tile dimensions
sq = pygame.Surface((wh[0], wh[1]))
sqrect = sq.get_rect()

#tempmovelist
templist = []
los = []

pygame.display.set_caption("pathfinding test")
     
def renderGui():
    gusq.top = 0
    gusq.left = 0
    screen.blit(gu, gusq)

def renderTitle():
    if pygame.font:
       text = font.render("pathfinding", 1, white)
       textpos = text.get_rect(centerx = size[0]/2, centery = guiheight/2)
       screen.blit(text, textpos)
    else:
        print ("not a sausage")
        
def renderMap():
    posx = 0
    posy = 0

    for row in map:
        my_ypos = posy * base_tile_size
        posx = 0
        for tile in row:
            my_xpos = posx * base_tile_size
            sqrect.top = my_ypos+mapoff
            sqrect.left = my_xpos+hoff
            mytile = t_types[tile.get_tiletype()]
           
            screen.blit(mytile, sqrect)
            # render text for tile
            if pygame.font:
                x = str(posx)
                y = str(posy)
                te = tiletext.render(str(x+","+y), 1, white)
                tepos = te.get_rect(centerx=sqrect.left+16, centery=sqrect.top+10)
                screen.blit(te, tepos)

            posx = posx+1            
        posy = posy+1

def is_node_wall(node):
    nx = node.loc[0]
    ny = node.loc[1]
    if map[ny][nx].get_tiletype() == 6:
        return True
    else:
        return False

def is_wall(i, v):
        if map[v][i].get_tiletype() ==6:
            return True
        else:
            return False

def is_valid(i, v):
        if map[v][i].get_tiletype() ==6:
            return False
        else:
            return True

class Node(object):
    def __init__(self, c=0, p=None, l=None):
        self.cost = c
        self.parent = p
        self.loc = l
        self.g = 0
        self.h = 0
    
    def set_cost(self, c):
        self.cost = c
    
    def set_parent(self, p):
        self.parent = p

    def set_loc(self, l):
        self.loc = l
    
    def __eq__(self, other):
        return self.loc == other.loc
        
class Player(object):
    def __init__(self):
        self.pla = pygame.Surface((wh[0], wh[1]))
        self.plarect = self.pla.get_rect()    
        
        self.locx = 1
        self.locy = 1
        self.targetx = 1
        self.targety = 1
        
        self.running = False
    
    def set_target(self, targ):
        self.targetx = targ[0]
        self.targety = targ[1]
        self.calc_route(targ)
#        self.update_pos()
        renderTemplist()
        
    def calc_route(self, targ):
        t = targ # setup target
        l = int(self.locx), int(self.locy) # setup location
        currentnode = Node(l = l)
        currentnode.g = currentnode.h = currentnode.cost = 0
        print("current is {0} ".format(currentnode.loc))
        endtarget = int(targ[0]), int(targ[1])
        endnode = Node(l = endtarget)
        endnode.g = endnode.h = endnode.cost = 0
        print("end is {0} ".format(endnode.loc))
        if is_node_wall(endnode):
            print ("that's a wall")
            return True
        
        clist =[] # closedlist
        olist = []
        olist.append(currentnode)
        
        while len(olist) > 0: #loop open list
            currentnode = olist[0]
            currentindex = 0
            for index, item in enumerate(olist): # check open list for cheapest node
                if item.cost < currentnode.cost:
                    currentnode = item
                    currentindex = index
            olist.pop(currentindex)
            #print("olist length")
            #print(len(olist))
            clist.append(currentnode) # stick cheapest on the closedlist
           # print("clist length")
            #print(len(clist))
           # print (currentnode.loc)
           # print (endnode.loc)
           # print (currentnode == endnode)
            
            if currentnode == endnode:
                self.locx = currentnode.loc[0]
                self.locy = currentnode.loc[1]
                print("all done");
                self.running = True
                path = []
                curr = currentnode
                while curr is not None:
                    path.append(curr.loc)
                    curr = curr.parent
                global templist
                templist = path[::-1] # Return reversed path
                return True

            children = []
            for new_loc in [(0, -1), (0, 1), (-1, 0), (1, 0)]:#, (-1, -1), (-1, 1), (1, -1), (1, 1)]: # Adjacent squares
                node_loc = (currentnode.loc[0] + new_loc[0], currentnode.loc[1] + new_loc[1])
                if node_loc[0] > (len(map[0]) - 1) or node_loc[0] < 0 or node_loc[1] > (len(map) -1) or node_loc[1] < 0: # in map
                    print("Your out of limits")
                    continue
                if map[node_loc[1]][node_loc[0]].get_tiletype() == 6:
                    #print("it's a wall")
                    continue
                new_node = Node(p=currentnode, l = (node_loc[0], node_loc[1]))
                children.append(new_node)
           # print("child count")
          #  print(len(children))

            for child in children:
              #  print(child.loc)
                for closed_child in clist:
                    if child == closed_child:
                        continue

                child.g = currentnode.g + 1
                child.h = child.h = ((child.loc[0] - endnode.loc[0]) ** 2) + ((child.loc[1] - endnode.loc[1]) ** 2)
                child.cost = child.g + child.h

                for open_node in olist:
                     if child == open_node and child.g > open_node.g:
                        continue

                # Add the child to the open list
                olist.append(child)

    
    
    def update_pos(self):
        if p.running:
            global templist
            for i in range(len(templist)):
                mynode = templist[i]
                print("moves list")
                print(mynode)
                self.locx = int(mynode[0])
                self.locy = int(mynode[1])
                
               # while self.locx != mynode[0] and self.locy != mynode[1]:
               #     if self.locx != mynode[0]:
               #         if (mynode[0] - self.locx) > 0.01 or (self.locx - mynode[0]) > 0.01:
               #             self.locx += (mynode[0] - self.locx) * 0.02
               #         else:
               #             self.locx = mynode[0]
               #     if self.locy != mynode[1]:
               #         if (mynode[1] - self.locy) > 0.01 or (self.locy - mynode[1]) > 0.01:
               #             self.locy += (mynode[1] - self.locy) * 0.02
               #         else:
               #             self.locy = mynode[1]
                self.plarect.left = (self.locx*base_tile_size) + mapleft
                self.plarect.top = (self.locy*base_tile_size) + maptop
                screen.blit(self.pla, self.plarect)
                clock.tick(200)
            p.running = False
        else:
            self.plarect.left = (self.locx*base_tile_size) + mapleft
            self.plarect.top = (self.locy*base_tile_size) + maptop
            screen.blit(self.pla, self.plarect)        

def renderplayer():
    pth = pygame.Surface((wh[0], wh[1]))
    pthrect = pth.get_rect()

    pthrect.left = (p.locx*base_tile_size) + mapleft
    pthrect.top = (p.locy*base_tile_size) + maptop
    charac2 = pygame.transform.rotate(charac, 270)
    screen.blit(charac2, pthrect)

def renderTemplist():
    pth = pygame.Surface((wh[0], wh[1]))
    pthrect = pth.get_rect()
    pth.fill((0,0,255))
    pth.set_alpha(85)
    global templist
    if len(templist) > 0:
        myl = list(templist)
        a=0
        for x in myl:
            a=a+1
            pthrect.left = (x[0]*base_tile_size) + mapleft
            pthrect.top = (x[1]*base_tile_size) + maptop
            screen.blit(pth, pthrect)
            tilenum = font.render(str(a), 1, white)
            tilenumpos = tilenum.get_rect(centerx=pthrect.left+16, centery=pthrect.top+16)
            screen.blit(tilenum, tilenumpos)
        t = x[0], x[1]
    
        
    
def handleEvents():
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()
        
        elif event.type == pygame.MOUSEBUTTONDOWN:
            pass
        elif event.type == pygame.MOUSEBUTTONUP:            
            mpos = pygame.mouse.get_pos()
            print("clicked at {0}".format(mpos))
            if mpos[0] > mapleft and mpos[0] < mapright:
                print("within left right")
                if mpos[1] > maptop and mpos[1] < mapbottom:
                    print("within top bottom")
                    tpos = tw = int((mpos[0] - mapleft)/wh[0]), int((mpos[1] - maptop)/wh[1])
                    p.set_target(tpos)
                    print(tpos)
                    
        #keys = pygame.key.get_pressed()  #checking pressed keys
        #if keys[pygame.K_a]:
         #   xvel -= 1
        #elif keys[pygame.K_d]:
        #    xvel += 1
        #elif keys[pygame.K_w]:
        ##    yvel -= 1
        #elif keys[pygame.K_s]:
        #    yvel += 1
        
# main loop
p = Player()
while 1:
    clock.tick(60)
    #xvel -= 0.01
    #yvel -= 0.01
    renderGui()
    renderTitle()
    renderMap()
    handleEvents()
    renderTemplist()
    renderplayer()
    pygame.display.flip()
